#!/bin/bash

# File paths
user_file="usernames.txt"
pass_file="passwords.txt"

# Loop through usernames and passwords
while read -r username && read -r password <&3; do
    # Create user account
    useradd -m "$username"

    # Set user password
    echo "$password" | passwd --stdin "$username"

    # Output user account and password
    echo "User: $username"
    echo "Password: $password"
done <"$user_file" 3<"$pass_file"


